s # SLQ: Drupal 8 website

## Quick start

### Docker

Prerequisites: [Docker](https://www.docker.com/docker-mac) and [Docker compose](https://docs.docker.com/compose/install/) installed.

Verify that you have both. In your CLI run:

```
$ docker -v
Docker version 18.06.0-ce, build 0ffa825 # Can be different version.
$ docker-compose -v
docker-compose version 1.22.0, build f46880f # Can be different version.
```

Download and start containers:

```
docker-compose up -d --build
```

Verify that all services are up and running.

```
$ docker-compose ps
   Name                 Command               State               Ports             
------------------------------------------------------------------------------------
mysql        docker-entrypoint.sh mysqld      Up      3306/tcp                      
phpmyadmin   /run.sh supervisord -n           Up      0.0.0.0:5600->80/tcp, 9000/tcp
web          docker-php-entrypoint apac ...   Up      0.0.0.0:5678->80/tcp
``` 

Get composer dependencies and install the website

```
docker exec -it web composer install
docker exec -it web composer site-install-docker-ci
```

You can now access website via [URL](http://0.0.0.0:5678/) or run 
```
 docker exec -it web composer drush-uli-dockerci
```

to get one time admin link.

## Links

### General

* [Docs](./docs/README.md)

### Environment

* [Platform: dev](https://dev-54ta5gq-wjkvwuomrnslo.au.platformsh.site/)
* [Platform: stage](https://stage-y77w3ti-wjkvwuomrnslo.au.platformsh.site/)

### Development

* [Slack](https://drupalsouth.slack.com/messages/C9XFSPYQP/)
* [Platfrom.sh hosting](https://au.platform.sh/projects/wjkvwuomrnslo/)
* [Gitlab](https://gitlab.com/drupal-south/drupalsouth-d8)

### Repos: Git

* GitLab: `git@gitlab.com:drupal-south/drupalsouth-d8.git`
* Platform.sh: `wjkvwuomrnslo@git.au.platform.sh:wjkvwuomrnslo.git`
