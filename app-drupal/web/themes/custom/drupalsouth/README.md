

1. Run below command to install all the required packages with Node Package Manager.

    ```
    npm install
    ```

1. Start up watches,

    ```
    gulp
    ```

3. For just compile style

    ```
    gulp sass-watch
    ```
