#!/usr/bin/env bash
# v.1.0

DRUSH="./vendor/bin/drush"
if [ "$[ $(grep "^====" web/ -R --include \*.html --include \*.css --include \*.scss --include \*.js | wc -l) + 0]" -ne "0" ]; then
  echo "ERROR: Conflicts found."
  grep "^====" web/ -R --include \*.html --include \*.css --include \*.scss --include \*.js
  exit 1
fi
