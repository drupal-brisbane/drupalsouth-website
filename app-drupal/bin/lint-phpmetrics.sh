#!/usr/bin/env bash
CRED=`tput setaf 1`
CGREEN=`tput setaf 2`
CRESET=`tput sgr0`

# Drupal 8 conf: file placement: blocks
PHPMETRICS=$($COMPOSER_HOME/vendor/bin/phpmetrics web/modules/custom/)
echo "${PHPMETRICS}"
if [ "$(echo "$PHPMETRICS" | grep "Error                                       0" | wc -l)" -ne "1" ]; then
  echo -e "${CRED}ERROR${CRESET}: Errors in php metrics. See details above."
  return 1
fi
