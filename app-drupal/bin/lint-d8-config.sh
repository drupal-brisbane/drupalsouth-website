#!/usr/bin/env bash
# v.1.2
DOCROOT="web"
PREFIX="dsconf"
THEME="drupalsouth"
THEME2="bootstrap4"

# Drupal 8 config: block naming conventions.
function d8lint-check-block-name () {
  echo "Lint: Drupal 8 config: block naming conventions."

  BLOCKS=$(find $DOCROOT -name "block.block.*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*")
  BLOCKS_COUNT=$(find $DOCROOT -name "block.block.*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*" | wc -l)

  BLOCKS_CORRECT=$(find $DOCROOT \( -name "block.block.${THEME}_*.yml" -o  -name "block.block.${THEME2}_*.yml" -o  -name "block.block.bootstrap_*.yml" -o -name "block.block.seven_*.yml" \) -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*")
  BLOCKS_CORRECT_COUNT=$(find $DOCROOT \( -name "block.block.${THEME}_*.yml" -o -name "block.block.${THEME2}_*.yml" -o -name "block.block.bootstrap_*.yml" -o -name "block.block.seven_*.yml" \) -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*" | wc -l)

  if [ "$BLOCKS_COUNT" -ne "$BLOCKS_CORRECT_COUNT" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: File naming conventions: blocks. Blocks must start with enabled theme name followed by underscore e.g. block.block.seven_....yml. Details: "
    diff -a  <(echo "$BLOCKS") <(echo "$BLOCKS_CORRECT")
    return 1
  fi
  return 0
}

# Drupal 8 config: taxonomy naming conventions.
function d8lint-check-taxonomy-name () {
  echo "Lint: Drupal 8 config: taxonomy naming conventions."

  TAXON=$(find $DOCROOT -name "taxonomy.vocabulary.*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*")
  TAXON_COUNT=$(find $DOCROOT -name "taxonomy.vocabulary.*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*" | wc -l)

  TAXON_CORRECT=$(find $DOCROOT -name "taxonomy.vocabulary.${PREFIX}_*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*")
  TAXON_CORRECT_COUNT=$(find $DOCROOT -name "taxonomy.vocabulary.${PREFIX}_*.yml" -not -path "${DOCROOT}/themes/contrib/*" -not -path "${DOCROOT}/modules/contrib/*" -not -path "${DOCROOT}/core/*" | wc -l)

  if [ "$TAXON_COUNT" -ne "$TAXON_CORRECT_COUNT" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: File naming conventions: taxonomy. Taxonomy vocabularies must start with '$PREFIX' followed by underscore e.g. taxonomy.vocabulary.${PREFIX}_....yml. Details: "
    diff -a  <(echo "$TAXON") <(echo "$TAXON_CORRECT")
    return 1
  fi
  return 0
}

# Drupal 8 config: blocks configuration placement.
function d8lint-check-block-placement () {
  echo "Lint: Drupal 8 config: blocks configuration placement."

  BLOCKS_OUTSIDE_COUNT=$(find $DOCROOT -name "block.block.*.yml" -path "${DOCROOT}/modules/custom/*" -path "${DOCROOT}/profiles/*" | wc -l | sed 's/^ *//')
  if [ "$BLOCKS_OUTSIDE_COUNT" -ne "0" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: File placement: blocks. Blocks configuration must be in theme. Details: "
    echo $(find $DOCROOT -name "block.block.*.yml" -path "${DOCROOT}/modules/custom/*" -path "${DOCROOT}/profiles/*")
    return 1
  fi
  return 0
}

# Drupal 8 config: uuid and core configuration artefacts.
function d8lint-check-config-artefacts () {
  echo "Lint: Drupal 8 config: uuid and core configuration artefacts."

  UUID_COUNT=$(grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=\*.{yml,yaml} --exclude=responsive_image.styles.* -e '^uuid:'  -e 'default_config_hash:' -e '^_core:' | wc -l | sed 's/^ *//')
  
  if [ "$UUID_COUNT" -ne "0" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: Found configuration artifacts. Details: "
    grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=\*.{yml,yaml} --exclude=responsive_image.styles.* -e '^uuid:'  -e 'default_config_hash:' -e '^_core:'
    return 1
  fi
  return 0
}

# Drupal 8 config: view dispaly naming conventions.
function d8lint-check-view-display-name () {
  echo "Lint: Drupal 8 config: view dispaly naming conventions."

  VIEWS_COUNT=$(grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=*views.view.\*.{yml,yaml} -e 'page_\d\+:$' -e 'page_\d\+$' -e 'block_\d\+:$' -e 'block_\d\+$' | wc -l)

  if [ "$VIEWS_COUNT" -ne "0" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: View display names should not use generic names. Details: "
    grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=*views.view.\*.{yml,yaml} -e 'page_\d\+:$' -e 'page_\d\+$' -e 'block_\d\+:$' -e 'block_\d\+$'
    return 1
  fi
  return 0
}

# Drupal 8 config: empty descriptions.
function d8lint-check-empty-description () {
  echo "Lint: Drupal 8 config: empty descriptions."

  VIEWS_COUNT=$(grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=*{views.view,node.type,paragraphs.paragraphs_type}.\*.{yml,yaml} -e "^description: ''$" | wc -l)

  if [ "$VIEWS_COUNT" -ne "0" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 config: Found empty descriptions. Details: "
    grep -rnw "${DOCROOT}/modules/custom" -rnw "${DOCROOT}/themes/custom" -rnw "${DOCROOT}/profiles" --include=*{views.view,node.type,paragraphs.paragraphs_type,taxonomy.vocabulary}.\*.{yml,yaml} -e "^description: ''$"
    return 1
  fi
  return 0
}

# Custom modules: README.md not found.
function d8lint-check-module-structure () {
  README_LIST=$(find ${DOCROOT}/modules/custom -name README.md -maxdepth 2)
  README_COUNT=$(echo "$README_LIST" | wc -l)
  MODULE_LIST=$(find ${DOCROOT}/modules/custom -mindepth 1 -maxdepth 1 -type d | sed 's/$/\/README.md/')
  MODULE_COUNT=$(echo "$MODULE_LIST" | wc -l)
  if [ "$README_COUNT" -ne "$MODULE_COUNT" ]; then
    echo -e "=======\n<ERROR>\nDrupal 8 structure: README.md file not found in all custom modules. Files missing: "
    diff -a  <(echo "$MODULE_LIST") <(echo "$README_LIST") | sed -n '1!p'
    return 1
  fi
  return 0
}

error=0
d8lint-check-block-name
error=`expr $error + $?`
d8lint-check-taxonomy-name
error=`expr $error + $?`
d8lint-check-block-placement
error=`expr $error + $?`
d8lint-check-config-artefacts
error=`expr $error + $?`
d8lint-check-view-display-name
error=`expr $error + $?`
d8lint-check-empty-description
error=`expr $error + $?`
d8lint-check-module-structure
error=`expr $error + $?`

exit $error
