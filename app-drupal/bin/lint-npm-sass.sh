#!/usr/bin/env bash
# v.1.0

CRED=`tput setaf 1`
CGREEN=`tput setaf 2`
CRESET=`tput sgr0`

# Drupal 8 conf: file placement: blocks
SASS_LINT_COUNT=$(npm run sass-lint | wc -l | sed 's/^ *//')
if [ "$SASS_LINT_COUNT" -ne "4" ]; then
  echo -e "${CRED}ERROR${CRESET}: Sass issues. Details: "
  npm run sass-lint
  exit 1
fi
