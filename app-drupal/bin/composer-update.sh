#!/usr/bin/env bash

composer update composer/composer drupal/core-recommended drupal/core-dev --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/core (9.2.0 => 9.2.2). ~rn"
composer update drupal/backup_migrate --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/backup_migrate (5.0.0 => 5.0.1). ~rn"
composer update drupal/bootstrap4 --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/bootstrap4 (2.1.11 => 2.1.13). ~rn"
composer update drupal/bootstrap5 --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/bootstrap5 (1.0.0-beta3 => 1.0.0-beta5). ~rn"
composer update drupal/webform --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/webform (6.0.3 => 6.0.4). ~rn"
composer update drupal/ds --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/ds (3.12.0 => 3.13.0). ~rn"
composer update drupal/entity_browser --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/views_bulk_operations (3.13.0 => 4.0.0). ~rn"
composer update drupal/entity_browser_entity_form --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/entity_browser_entity_form (2.5.0 => 2.6.0). ~rn"
composer update drupal/migrate_tools --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/migrate_tools (dev-5.x 9d9c02f => dev-5.x 56d82b4). ~rn"
composer update drupal/search_api --with-dependencies && git add composer.* && git commit -m "[#1] Upgrading drupal/search_api (1.19.0 => 1.20.0). ~rn"

composer update && git add composer.* && git commit -m "[#1] Miscelaneous updates. ~rn"

composer prod-copy-config
git add config && git commit -m "Updated config."

composer drush-updb

git push gitlab && git push origin
