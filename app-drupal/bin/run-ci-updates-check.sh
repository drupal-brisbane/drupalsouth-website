#!/usr/bin/env bash
# v2.0

# Variables.
ISSUEID="1"
ALIAS="default"
LOGS="$(pwd)/web/sites/$ALIAS/files/composer-update"
rm ${LOGS}*.log || echo -e "Logs not found in ${LOGS}"

# Get updates.
./vendor/bin/composer update --dry-run --no-ansi 2>"${LOGS}.log"
echo -e "##############\n##############\nRESULT: composer update\n##############\n"
cat "${LOGS}.log"

echo -e "##############\n##############\nBASH SCRIPT\n##############\n"

VALIDUPDATES=$(cat ${LOGS}.log | grep "\- Upgrading dru" | grep -v "Upgrading drupal/core\-")
SSHUPDATES=$(echo -e "$VALIDUPDATES" | sed -e "s#[[:space:]]*- Upgrading \([^[:space:]]*\)#composer update \1 --with-dependencies \&\& git add composer.* \&\& git commit -m \"\[\#$ISSUEID\] Upgrading \1#g" | sed -e "s#update drupal/core#update composer/composer drupal/core-recommended drupal/core-dev#g" | sed -e "s#\$#\. ~rn\"#g")
echo -e "$SSHUPDATES"
#cat ${LOGS}-bash.log

rm ${LOGS}*.log || echo -e "Logs not found in ${LOGS}"

echo -e "##############\n##############\nSECURITY LOG\n##############\n"
./vendor/bin/drush -l $ALIAS  pm:security
