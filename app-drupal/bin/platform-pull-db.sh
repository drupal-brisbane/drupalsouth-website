#!/usr/bin/env bash
# v.1.3

# Check parameters.
USAGE="Usage: ./platform-pull-db.sh clent.\nUse <skipssh> to use local database.\nExamples:\n  bash ./bin/platform-pull-db.sh\n  bash ./bin/platform-pull-db.sh skipssh"
#if [ $# -lt 1 ]; then
#  echo -e "$USAGE"
#  exit 1
#fi

HOSTDB="default"

#if [ "$1" = "drupalsouth" -o "$1" = "drupalsouth2021" ]; then
#  HOSTDB="$1"
#fi
#if [ "$2" = "drupalsouth" -o "$2" = "drupalsouth2021" ]; then
#  HOSTDB="$2"
#fi

HOSTSERVER="wjkvwuomrnslo-master-7rqtwti--app-drupal@ssh.au.platform.sh"
#FLDR="$(cd ~ && pwd)"
FLDR="../_db/${HOSTDB}"

#rm -rf ./_db
mkdir -p $FLDR

if [ "$1" != "skipssh" ]; then
  # Periodic backups.
  ssh -o StrictHostKeyChecking=no $HOSTSERVER "ls -1t /app/private/backup_migrate/backup*sql.gz | head -1"
    REMOTEFILE=$(ssh -o StrictHostKeyChecking=no $HOSTSERVER "ls -1t /app/private/backup_migrate/backup*sql.gz | head -1") && echo $REMOTEFILE && scp -r $HOSTSERVER:$REMOTEFILE $FLDR
fi

chmod 777 ${FLDR}/*sql*

composer site:restore-db
composer drush-uli-local

# rm -rf ./_db
