# DrupalSouth

Drupal 9 website for [DrupalSouth](https://drupalsouth.org/) conference

## Getting started

All communication via Google Chat: https://mail.google.com/chat/u/0/#chat/space/AAAAR6_7BYc

1. Please register and send me your Gitlab account https://gitlab.com/
* Gitlab repo: https://gitlab.com/drupal-brisbane/drupalsouth-website
* Gitlab issue queue: https://gitlab.com/drupal-brisbane/drupalsouth-website/-/boards

2. Watch videos:
- About the project: https://www.youtube.com/watch?v=6gmkbjIlgGw
- Ways of working: https://youtu.be/vkWI_C-NlR4

3. DrupalSouth website:
- Current: https://drupalsouth.org/
- New: https://dev-54ta5gq-wjkvwuomrnslo.au.platformsh.site/

4. Development instructions:
- Getting started: https://gitlab.com/drupal-brisbane/drupalsouth-website/-/blob/main/README.md

5. Register on drupal.org to receive your credit.
- Registration: https://www.drupal.org/user/register
- List your tasks here: https://www.drupal.org/project/drupalsouth/issues/3225969

## Development

* App root: `app-drupal`
* Web root: `app-drupal/web`
* Unused static web root: `app-static`

### Lando

Install [lando](https://docs.lando.dev/basics/installation.html). Run:

```
lando start

# Get dependencies
lando composer install

# Restore database
lando composer site:restore-db

# Login to website
lando info
lando drush uli -l [URL-FROM-DRUSH-INFO]
# OR lando drush -l $(lando info --path "[0]['urls'][2]" | sed s#\'##g) user:login 
```
